#!/usr/bin/env python
#
# python script to prepar the injection of vsphere clusters
# inside netbox

#
from st2common.runners.base_action import Action

# def vsphere_clusters_esxi(esxi_hosts):

#     clusters = {}
#     for platform in esxi_hosts:
#         for i in platform.keys():
#             domain = platform[i]['Parent'].split(':')[1]
#             if domain not in clusters.keys():
#                 clusters[domain] = [ i ]
#             else:
#                 clusters[domain].append( i )
#     return clusters

# def vsphere_clusters(vsphere_clusters):
#     clusters = []
#     for vcenter in vsphere_clusters:
#         for i in vcenter.keys():
#             cluster.append(
#                 {
#                     'cluster_name': i,
#                     'cluster_oid': vcenter[i]['ID']
#                 }
#             )
#     return(clusters)


class PrintConfigAction(Action):
    def run(self, **kwargs):

        '''
        # print('netbox_clusters-0')
        # print(kwargs['netbox_clusters'][0])
        # print('netbox_esxi-0')
        # print(kwargs['netbox_esxi'][0])
        # print('esxi_hosts-0')
        # print(kwargs['esxi_hosts'].keys())
        # print('vsphere_clusters-0')
        # print(kwargs['vsphere_clusters'].keys())

        netbox_esxi = [ [ {'name': 'device1', ...}, {'name: 'device2', ...} ],
                        [ {'name': 'device3', ...}, {'name: 'device4', ...} ],
                      ]

        esxi_hosts = [ [ { 'device1': { 'ID': oid, 'Parent': oid }, 
                         { 'device2': { 'ID': oid, 'Parent': oid } ],
                       [ { 'device3': { 'ID': oid, 'Parent': oid }, 
                         { 'device4': { 'ID': oid, 'Parent': oid } ]
                     ]

        netbox_clusters = [ { 'name': 'cluster1'}, { 'name': 'cluster2' } ]
        vsphere_clusters = [ [ { 'cluster1': { 'ID': oid, 'Parent': oid }, 
                               { 'cluster2': { 'ID': oid, 'Parent': oid } ],
                             [ { 'cluster3': { 'ID': oid, 'Parent': oid }, 
                               { 'cluster4': { 'ID': oid, 'Parent': oid } ]
                           ]
        '''
        ###################
        ## esxi hosts
        
        # # create a netbox esxi hosts list
        # netbox_esxi = []
        # for i in kwargs['netbox_esxi']:
        #     netbox_esxi+=i

        # clusters from netbox
        compare_netbox_clusters = []
        compare_vsphere_clusters = []

        create_vsphere_clusters = []
        new_create_vsphere_clusters = []
        
        remove_vsphere_clusters = []


        # esxi_host same cluster
        esxi_hosts = []

        # netbox_clusters
        # print('netbox_clusters')
        # print(kwargs['netbox_clusters'])
        # print(len(kwargs['netbox_clusters']))

        for i in kwargs['netbox_clusters']:
            print(i)
            if i['group']['name'] == kwargs['vcenter']:
                nb_cluster = {
                    'id': i['id'],
                    'name': i['name'],
                    'custom_fields': i['custom_fields'],
                    'site': i['site']['id'],
                    'group': i['group']['id']
                }

                # cluster not found inside vsphere output
                if i['name'] not in kwargs['vsphere_clusters'].keys():
                    remove_vsphere_clusters.append(nb_cluster)

                compare_netbox_clusters.append(nb_cluster)

        # #################
        for i in kwargs['vsphere_clusters'].keys():
            create = True
            vp_cluster = {
                'name': i,
                'custom_fields': {
                    'oid': kwargs['vsphere_clusters'][i]['ID'].split(':')[1].replace("'","")
                }
            }
            compare_vsphere_clusters.append(vp_cluster)

            # needed create?
            for cluster in compare_netbox_clusters:
                if i == cluster['name']:
                    if vp_cluster['custom_fields']['oid'] == cluster['custom_fields']['oid']:
                        create = False

            # needed create? = Yes
            if create == True:
                create_vsphere_clusters.append(vp_cluster)

        for i in create_vsphere_clusters:
            # i['custom_fields'] = {
            #     'oid': kwargs['vsphere_clusters'][i]['ID'].split(':')[1]
            # }

            for esxi_host in kwargs['esxi_hosts'].keys():
                # print('debug ----')
                # print(kwargs['esxi_hosts'][esxi_host]['Parent'].split(':')[1].replace("'",""))
                # print(i['custom_fields']['oid'])
                if kwargs['esxi_hosts'][esxi_host]['Parent'].split(':')[1].replace("'","") == i['custom_fields']['oid']:
                    i['host'] = esxi_host.split('.')[0].upper()

                    # only clusters with hosts can be created!
                    new_create_vsphere_clusters.append(i)
                    break
                    
                    # for nb_host in netbox_esxi:
                    #     # print('debug ----')
                    #     # print(nb_host['name'].upper())
                    #     # print(esxi_host.split('.')[0].upper())
                    #     try:
                    #         if nb_host['name'].upper() == esxi_host.split('.')[0].upper():
                    #             i['site'] = nb_host['site']['id']
                    #             break
                    #     except:
                    #         pass

        for esxi_host in kwargs['esxi_hosts'].keys():
            kwargs['esxi_hosts'][esxi_host]['Parent'] = kwargs['esxi_hosts'][esxi_host]['Parent'].split(':')[1].replace("'","")
            kwargs['esxi_hosts'][esxi_host]['ID'] = kwargs['esxi_hosts'][esxi_host]['ID'].split(':')[1].replace("'","")
            
            for nb_cluster in kwargs['netbox_clusters']:
                if nb_cluster['custom_fields']['oid'] == kwargs['esxi_hosts'][esxi_host]['Parent']:

                    # return only hosts of same clusters
                    kwargs['esxi_hosts'][esxi_host]['cluster_id'] = nb_cluster['id']
                    kwargs['esxi_hosts'][esxi_host]['hostname'] = esxi_host
                    esxi_hosts.append(kwargs['esxi_hosts'][esxi_host])
                    
            
        output = {
            'clusters_create': new_create_vsphere_clusters,
            'clusters_remove': remove_vsphere_clusters,
            'esxi_hosts': esxi_hosts
        }

        if len(create_vsphere_clusters) > 0 or len(remove_vsphere_clusters) > 0:
            output['task'] = 1
        else:
            output['task'] = 0
            
        return (True, output)
  #       ### VSPHERE
  #       # create a vsphere clusters
  #       #print(vsphere_clusters(kwargs['vsphere_clusters']))
  #       #try vsphere_clusters
  #       #print(vsphere_clusters_esxi(kwargs['esxi_hosts']))

  #       ###################
  #       ## esxi hosts
        
  #       # create a netbox esxi hosts list
  #       netbox_esxi_full = []
  #       netbox_esxi = []        
  #       for i in kwargs['netbox_esxi']:
  #           netbox_esxi_full+=i

  #       for i in netbox_esxi_full:
  #           netbox_esxi.append( i['name'] )
            
  #       # create a vsphere esxi host list
  #       esxi_hosts = []
  #       for i in kwargs['esxi_hosts']:
  #           esxi_hosts+= list(i.keys())
  #       esxi_hosts = [host.split('.')[0].upper() for host in esxi_hosts]

  #       # sort keys
  #       netbox_esxi.sort()
  #       esxi_hosts.sort()

  #       print({
  #           'netbox_esxi': netbox_esxi,
  #           'esxi_hosts': esxi_hosts
  #       })

  #       ## end esxi hosts
  #       ###################

  #       ###################
  #       ## vsphere clusters
  #       vsphere_clusters = []
  #       for i in kwargs['vsphere_clusters']:
  #           vsphere_clusters+= list(i.keys())

        
  #       netbox_clusters = []
  #       for i in kwargs['netbox_clusters']:
  #           netbox_clusters.append( i['name'] )

  #       vsphere_clusters.sort()
  #       netbox_clusters.sort()

  #       print({
  #           'vsphere_clusters': vsphere_clusters,
  #           'netbox_clusters': netbox_clusters
  #       })

  #       #
  #       #print({"netbox_esxi": netbox_esxi,
  #       #       "esxi_hosts": esxi_hosts})

  #       if netbox_esxi == esxi_hosts:
  #           esxi_status = True
  #           esxi_message = 'esxi: consistent data'
  #       else:
  #           esxi_status = False
  #           esxi_message = 'esxi: inconsistent data'
            
  #       if vsphere_clusters == netbox_clusters:
  #           cluster_status = True
  #           cluster_message = 'vsphere clusters: consistent data'
  #       else:
  #           cluster_status = False
  #           cluster_message = 'vsphere clusters: inconsistent data'

  #       telegram_output = '''
  # |VSPHERE + Netbox| 
  # %s; 
  # %s ;''' % (esxi_message, cluster_message)

  #       if cluster_status == True and esxi_status == True:
  #           return(True, telegram_output)
  #       else:
  #           return (False, telegram_output)

# def vsphere_clusters_esxi(vsphere_clusters, esxi_hosts):
#     clusters = []
#     for vcenter in vsphere_clusters:
#         for i in vcenter.keys():
#             clusters.append(
#                 {
#                     'cluster_name': i,
#                      'cluster_oid': vcenter[i]['ID'].split(':')[1]
#                     'hosts': [
#                         {'hostname': 'g202002',
#                          'host_oid': 'vskjn'}]
#                 }
#             )

#     clusters = {}
#     for platform in esxi_hosts:
#         for i in platform.keys():
#             cluster_oid = platform[i]['Parent'].split(':')[1]

#             for cluster in clusters:
#                 if cluster[cluster_oid] == cluster_oid:
#                     cluster[hosts]

#             if domain not in clusters.keys():
#                 clusters[domain] = [ i ]
#             else:
#                 clusters[domain].append( i )
#     return clusters
