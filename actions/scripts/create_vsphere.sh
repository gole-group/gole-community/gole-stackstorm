#!/bin/bash

case $3 in
    *vcenter*)
        VCENTER=$(echo $3 | cut -d"-" -f1)
        #
        echo '
  '$VCENTER':
    host: '$VCENTER'
    passwd: '$2'
    port: 443
    user: '$1'
' > /tmp/$VCENTER.tmp
    ;;
esac
