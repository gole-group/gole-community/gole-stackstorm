#!/usr/bin/env python
from st2common.runners.base_action import Action

def vsphere_clusters_esxi(esxi_hosts):

    clusters = {}
    for platform in esxi_hosts:
        for i in platform.keys():
            domain = platform[i]['Parent'].split(':')[1]
            if domain not in clusters.keys():
                clusters[domain] = [ i ]
            else:
                clusters[domain].append( i )
    return clusters

def vsphere_clusters(vsphere_clusters):

    clusters = []
    for vcenter in vsphere_clusters:
        for i in vcenter.keys():
            cluster.append(
                {
                    'cluster_name': i,
                    'cluster_oid': vcenter[i]['ID']
                }
            )
    return(clusters)

class PrintConfigAction(Action):
    def run(self, **kwargs):

        '''
        netbox_esxi = [ [ 'device1', 'device2' ], [ 'device3', 'device4' ] ]
        esxi_hosts = [ [ { 'device1': { 'ID': oid, 'Parent': oid }, 
                         { 'device2': { 'ID': oid, 'Parent': oid } ],
                       [ { 'device3': { 'ID': oid, 'Parent': oid }, 
                         { 'device4': { 'ID': oid, 'Parent': oid } ]
                     ]

        netbox_clusters = [ { 'name': 'cluster1'}, { 'name': 'cluster2' } ]
        vsphere_clusters = [ [ { 'cluster1': { 'ID': oid, 'Parent': oid }, 
                               { 'cluster2': { 'ID': oid, 'Parent': oid } ],
                             [ { 'cluster3': { 'ID': oid, 'Parent': oid }, 
                               { 'cluster4': { 'ID': oid, 'Parent': oid } ]
                           ]

        '''

        ### VSPHERE
        # create a vsphere clusters
        #print(vsphere_clusters(kwargs['vsphere_clusters']))
        #try vsphere_clusters
        #print(vsphere_clusters_esxi(kwargs['esxi_hosts']))

        ###################
        ## esxi hosts
        
        # create a netbox esxi hosts list
        netbox_esxi_full = []
        netbox_esxi = []        
        for i in kwargs['netbox_esxi']:
            netbox_esxi_full+=i

        for i in netbox_esxi_full:
            netbox_esxi.append( i['name'] )
            
        # create a vsphere esxi host list
        esxi_hosts = []
        for i in kwargs['esxi_hosts']:
            esxi_hosts+= list(i.keys())
        esxi_hosts = [host.split('.')[0].upper() for host in esxi_hosts]

        # sort keys
        netbox_esxi.sort()
        esxi_hosts.sort()

        print({
            'netbox_esxi': netbox_esxi,
            'esxi_hosts': esxi_hosts
        })

        ## end esxi hosts
        ###################

        ###################
        ## vsphere clusters
        vsphere_clusters = []
        for i in kwargs['vsphere_clusters']:
            vsphere_clusters+= list(i.keys())

        
        netbox_clusters = []
        for i in kwargs['netbox_clusters']:
            netbox_clusters.append( i['name'] )

        vsphere_clusters.sort()
        netbox_clusters.sort()

        print({
            'vsphere_clusters': vsphere_clusters,
            'netbox_clusters': netbox_clusters
        })

        #
        #print({"netbox_esxi": netbox_esxi,
        #       "esxi_hosts": esxi_hosts})

        if netbox_esxi == esxi_hosts:
            esxi_output = ""
            esxi_status = True
            esxi_message = 'esxi: consistent data'
        else:

            esxi_output = '''
ESXI FAIL:
+---------------
Netbox: %s
+---------------
Vsphere: %s
+---------------
''' % ( list(set(netbox_esxi) - set(esxi_hosts)), list(set(esxi_hosts) - set(netbox_esxi)) )

            esxi_status = False
            esxi_message = 'esxi: inconsistent data'
            
        if vsphere_clusters == netbox_clusters:
            cluster_output = ""            
            cluster_status = True
            cluster_message = 'vsphere clusters: consistent data'
        else:

            cluster_output = '''
CLUSTER FAIL:
+---------------
Netbox: %s
+---------------
Vsphere: %s
+---------------
''' % ( list(set(netbox_clusters) - set(vsphere_clusters)), list(set(vsphere_clusters) - set(netbox_clusters)) )
            
            cluster_status = False
            cluster_message = 'vsphere clusters: inconsistent data'

        if cluster_status == True and esxi_status == True:
            
            return(True, "VSPHERE + Netbox - CLUSTERS and ESXI OK")
        else:
            telegram_output = '''%s
%s''' % ( esxi_output, cluster_output )
            
            return (False, telegram_output)

# def vsphere_clusters_esxi(vsphere_clusters, esxi_hosts):
#     clusters = []
#     for vcenter in vsphere_clusters:
#         for i in vcenter.keys():
#             clusters.append(
#                 {
#                     'cluster_name': i,
#                      'cluster_oid': vcenter[i]['ID'].split(':')[1]
#                     'hosts': [
#                         {'hostname': 'g202002',
#                          'host_oid': 'vskjn'}]
#                 }
#             )

#     clusters = {}
#     for platform in esxi_hosts:
#         for i in platform.keys():
#             cluster_oid = platform[i]['Parent'].split(':')[1]

#             for cluster in clusters:
#                 if cluster[cluster_oid] == cluster_oid:
#                     cluster[hosts]

#             if domain not in clusters.keys():
#                 clusters[domain] = [ i ]
#             else:
#                 clusters[domain].append( i )
#     return clusters
