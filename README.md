StackStorm + goLe actions integrations
======================================


### VSPHERE <--- Stackstorm ---> Netbox

![vsphere](img/vmware_vsphere.png "vsphere")   <---  ![StackStorm](img/ss.png "stackstorm")   --->   ![Netbox](img/netbox_logo.png "Netbox")

![gole](img/gole64x64.png "GOLE")

## Actions
* `gole.netbox-2-vsphere-config` - Create a stackstorm vsphere.yaml based on netbox cluster groups
* `gole.get-online-vcenters.netbox-vsphere` - Output a array with all online vcenters
* `gole.vsphere.netbox.create-clusters` - Create/Update all vsphere clusters inside netbox
* `gole.orquesta-vsphere-netbox-mirror` - Check if Netbox is a Vsphere true mirror for ESXi and Clusters
* `gole.vsphere.netbox.update-esxi` - Update all esxi hosts inside netbox
* `gole.vsphere.netbox.update-vms` - Update all vsphere vms inside netbox

```
| gole.vsphere.get-clusters-hosts                                       | gole               | Output the physical esxi hosts by clusters and all clusters           |
| gole.vsphere.netbox.cluster-host                                      | gole               | Add hosts inside a netbox vsphere clusters                            |
```

## Install

### dependencies

```
$ st2 pack install https://github.com/StackStorm-Exchange/stackstorm-netbox
$ st2 pack install https://github.com/StackStorm-Exchange/stackstorm-telegram
$ st2 pack install https://github.com/StackStorm-Exchange/stackstorm-vsphere
```

### gole stackstorm module

```
$ st2 pack install https://gitlab.com/gole-group/gole-community/gole-stackstorm.git
```

## Usage

### Create a vsphere.yaml based on netbox cluster groups.

In order, to `gole.netbox-2-vsphere-config` successfull create a `vsphere.yaml` file, we need:
* Create all vsphere cluster-groups on Netbox
Inside Netbox. On Virtualization, Cluster Groups you need to create a item when the sctruct of your name is   "hostname" + "-" + "foo-bar" + "-vcenter"
* user read-only on vsphere APIs
* password used to connect on vsphere

```
$ st2 action execute gole.update-create.netbox-vmware-clusters
```

### Return all vcenters_connection status

```
$ st2 action execute gole.get-online-vcenters.netbox-vsphere

id: asdfasdfjasldfasdfasdf
action.ref: gole.get-vcenters.netbox-vsphere
parameters: None
status: succeeded (24s elapsed)
start_timestamp: Fri, 13 Sep 2019 18:48:10 UTC
end_timestamp: Fri, 13 Sep 2019 18:48:34 UTC
result:
  output:
    vcenters_result:
    - SERVER1-vcenter:541a2342-ce76-4027-8960-3c5c90ee4abc
    - SERVER2-vcenter:13ab66f3-7e8b-4697-84cf-819422a30abc
    - SERVER3-vcenter:f3006678-1d84-456b-9ecf-afe6318bfabc
    - SERVER4-vcenter:f112664e-1edb-4a8a-8897-3218e90acabc
    - SERVER5-vcenter:7ff4662b-b206-49ac-b248-98430c6b8abc
    - SERVER6-vcenter:2654533f-7788-4eed-9e87-0c55675f3abc
    - SERVER7-vcenter:b2b91338-657e-4c2f-8ac1-748c783e7abc
    - SERVER8-vcenter:293ca33c-769d-4d62-8236-7db8e3305abc
    - SERVER9-vcenter:None
    - SERVER10-vcenter:928b33fd-ea52-4f8b-b775-ffa54fbb6ab
    - SERVER11-vcenter:d513229e-903e-4864-9c67-016b04a98ab
    - SERVER12-vcenter:72d022e6-397a-46c7-a71e-4b03dbe4dab
    - SERVER13-vcenter:bcc6331f-426a-426b-8b19-0ac95f540ab
    - SERVER14-vcenter:541b221b-f630-463a-a1c4-fcfe4d875abc
```

### Create clusters based on Netbox clusters-groups

It's get all clusters groups that ended with a `-vcenter` on name.

This will check the connection. If successed, get all clusters, and post the clusters
information inside Netbox.


```
$ st2 action execute gole.vsphere.netbox.create-clusters  -h

Create/update all vsphere clusters inside netbox

Optional Parameters:
    cluster_type_id
        Type: integer
        Default: 2

    concurrency
        Type: integer
        Default: 1

    netbox_limit
        Type: integer
        Default: 1000

    notify
        List of tasks to trigger notifications for.
        Type: array
        Default: []

$ st2 action execute gole.vsphere.netbox.create-clusters  cluster_type_id=2 concurrency=4
```

### Update all esxi hosts inside netbox.

This script add the host oid

```
$ st2 action execute gole.vsphere.netbox.update-esxi
```

### Compare ESXI vsphere hosts and Netbox ESXI Hosts

We need to pass the esxi hosts platforms used on netbox.

```
st2 action execute gole.orquesta-vsphere-netbox-mirror netbox_platform=exsi-5-5-0,esxi-5-1-0,esxi-6-0-0
```